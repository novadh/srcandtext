%
% file: srcandtext.cls
%
% Source and Text: load source code and print
% its explanation.
%
% (C) 2020-2025 Nova de Hi.
%
% MIT license.
%
\ProvidesExplClass{srcandtext}
	{2025/02/13}
	{v0.9}
	{source and text class}

%\RequirePackage{xparse}
\RequirePackage{l3keys2e}

\keys_define:nn { srcandtextclass }
{
	paper 	.tl_set:N = \opt_paper_tl,
	math	.bool_set:N = \opt_math_bool,
%	tcolorbox	.bool_set:N = \opt_tcolorbox_bool,
	margin	.tl_set:N = \opt_margin_tl,
	extraopt .tl_set:N = \opt_extraopt_tl,
	parindent .dim_set:N = \opt_parindent_dim,
	parskip .skip_set:N = \opt_parskip_skip,
%	expl .bool_gset:N = \opt_expl_bool,
	bvlskip	.tl_set:N = \opt_bvlskip_tl,
}

\keys_set:nn { srcandtextclass }
{
	paper = a4paper,
	math = false,
%	tcolorbox = false,
	margin = {25mm,*,30mm,*},
	extraopt = {},
	parindent = 0pt,
	parskip = 5pt plus 2pt minus 1pt,
%	expl = false,
	bvlskip = 1.2
}

\ProcessKeysOptions { srcandtextclass }

\tl_set:No \l_loadoptions_tl { \opt_paper_tl }
\bool_if:NT \opt_math_bool
{
	\tl_put_right:No \l_loadoptions_tl { , amsmath }
}
\tl_put_right:No \l_loadoptions_tl { , \opt_extraopt_tl }

\exp_last_unbraced:NNo \LoadClass [ \l_loadoptions_tl ]{oblivoir}

\ifLuaOrXeTeX
\setmainfont{TeX~Gyre~Pagella}
\setkomainfont(Noto~Serif~CJK~KR)
\fi
\SetHangulspace{1.3}{1.1}

\exp_last_unbraced:NNo \parindent=\opt_parindent_dim
\exp_last_unbraced:NNo \parskip=\opt_parskip_skip

\RequirePackage{fapapersize}
\exp_args:Nx \usefapapersize {*,*,\opt_margin_tl}

\bool_if:NT \opt_math_bool
{
	\RequirePackage {ob-mathleading}
}

\RequirePackage[bvlskip=\opt_bvlskip_tl]{srcandtext}

\makeindex

\endinput

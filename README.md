# Source and Text

## About

A simple LaTeX class to typeset Expl3 textbook.

## Usage

```
\documentclass[<options>]{srcandtext}
```

Read the class documentation.
